#include <stdio.h>
#include <stdlib.h>
#include "film.c"
#include "include/film.h"

int rechercheTitre = 0;
int rechercheDate = 0;
int rechercheGenre = 0;
int rechercheRealisateur = 0;

static int menuMain()
{
    int choice = 0;
    int res;

    while (choice < 1 || choice > 3)
    {
        printf("----------------------------\n");
        printf("|      Menu principal :    |\n");
        printf("----------------------------\n");
        printf("| 1 : Enregistrer un Film  |\n");
        printf("| 2 : Bibliotheque de Film |\n");
        printf("| 3 : Quitter              |\n");
        printf("----------------------------\n");
        printf("Votre choix ? ");

        res = scanf("%d", &choice);
        while ((getchar()) != '\n');

        if (res != 1 || choice < 1 || choice > 3)
        {
            choice = 0;
            printf("Valeur incorrect. Appuyer pour continuer..");
            getchar();
            system("cls");
        }
    }
    return choice;
}


static int menuCollection()
{
    int choice = 0;
    int res;

    while (choice < 1 || choice > 8)
    {
        printf("-------------------------------------------------------\n");
        printf("|               Menu Bibliotheque de Film             |\n");
        printf("-------------------------------------------------------\n");
        printf("| 1 : Afficher la collection                          |\n");
        printf("| 2 : Affiche les informations d'un film              |\n");
        printf("| 3 : Modifier le nombre d'exemplaires d'un film      |\n");
        printf("| 4 : Afficher les films d'une categorie              |\n");
        printf("| 5 : Afficher les films avec minimum 2 exemplaires   |\n");
        printf("| 6 : Afficher les films du plus recent au plus vieux |\n");
        printf("| 7 : Afficher les films d'un realisateur             |\n");
        printf("| 8 : Retour                                          |\n");
        printf("-------------------------------------------------------\n");
        printf("Votre choix ? ");

        res = scanf("%d",&choice);
        while ((getchar()) != '\n');

        if (res != 1)
        {
            choice = 0;
            printf("Valeur incorrect. Appuyer pour continuer..");
            getchar();
            system("cls");
        }
    }
    return choice;
}

int main(int argc, char *argv[])
{
    film filmList[50];

    while(1)
    {
        main:
        switch (menuMain())
        {
            case 1:
                system("cls");
                addFilm(&filmList);
                goto main;

            case 2:
                collection:
                system("cls");
                switch (menuCollection())
                {
                    case 1:
                        system("cls");
                        printCollection(filmList);
                        goto collection;

                    case 2:
                        system("cls");
                        printFilm(filmList);
                        goto collection;

                    case 3:
                        system("cls");
                        modifyFilm(filmList);
                        goto collection;

                    case 4:
                        system("cls");
                        printCategory(filmList);
                        goto collection;

                    case 5:
                        system("cls");
                        printDouble(filmList);
                        goto collection;

                    case 6:
                        system("cls");
                        printAscendingDateOrder(filmList);
                        goto collection;

                    case 7:
                        system("cls");
                        printProducerFilms(filmList);
                        goto collection;

                    default:
                        system("cls");
                        break;
                }
                goto main;

            default:
                break;
        }

        break;
    }

    return 0;
}


