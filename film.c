#include "include/film.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int index = 0;

static void addFilm(film *filmList)
{
    char titleTmp[100];
    short yearTmp = -1;
    int found = 0;
    char choice = '\n';
    int res;

    printf("-------------------\n");
    printf("| Ajout d'un film |\n");
    printf("-------------------\n");

    printf("Entrez le titre du film : ");
    rewind(stdin);
    fgets(titleTmp, sizeof(titleTmp), stdin);

    // If not the first film
    if (index != 0)
    {
        // Check film already in the collection
        for (int i=0; i<index; i++)
        {
            if (strcmp(filmList[i].title, titleTmp) == 0)
            {
                found = 1;
                break;
            }
        }

        // Ask to add a copy to the film
        if (found == 1)
        {
            while (choice == '\n')
            {
                printf("Ce film est deja present voulez-vous ajouter un examplaire ? (y/n) : ");
                rewind(stdin);
                res = scanf("%c", &choice);
                while ((getchar()) != '\n');

                if (res != 1)
                {
                    printf("Valeur entrez incorrect\n\n");
                    choice = '\n';
                }
                else
                {
                    // Results depending on the decision
                    switch(toupper(choice))
                    {
                        case 'Y':
                            addNumberFilm(filmList, titleTmp);
                            printf("Exemplaire ajoute avec succes\n");
                            break;

                        case 'N':
                            break;

                        default:
                            printf("Valeur entrez incorrect\n\n");
                            choice = '\n';
                    }
                }
            }
        }
        else
        {
            // Film not found then we ask the next steps
            strcpy(filmList[index].title, titleTmp);

            while (yearTmp == -1)
            {
                printf("Entrez l'age du film en annee (0 pour cette annee) : ");
                rewind(stdin);
                res = scanf("%hd", &yearTmp);
                while ((getchar()) != '\n');

                if (res != 1 || yearTmp < 0)
                {
                    printf("Valeur entrez incorrect\n\n");
                    yearTmp = -1;
                }
                else
                {
                    filmList[index].year = yearTmp;
                }
            }

            printf("Entrez le type de film : ");
            rewind(stdin);
            fgets(filmList[index].type, sizeof(filmList[index].type), stdin);

            filmList[index].number = 1;

            printf("Entrez le nom du realisateur : ");
            rewind(stdin);
            fgets(filmList[index].producer, sizeof(filmList[index].producer), stdin);

            index ++;

            printf("\nFilm enregistrer avec succes\n");
        }
    }
    // Else first film, ask next steps
    else
    {
        strcpy(filmList[index].title, titleTmp);

        while (yearTmp == -1)
        {
            printf("Entrez l'age du film en annee (0 pour cette annee) : ");
            rewind(stdin);
            res = scanf("%hd", &yearTmp);
            while ((getchar()) != '\n');

            if (res != 1 || yearTmp < 0)
            {
                printf("Valeur entrez incorrect\n\n");
                yearTmp = -1;
            }
            else
            {
                filmList[index].year = yearTmp;
            }

        }

        printf("Entrez le type de film : ");
        rewind(stdin);
        fgets(filmList[index].type, sizeof(filmList[index].type), stdin);

        filmList[index].number = 1;

        printf("Entrez le nom du realisateur : ");
        rewind(stdin);
        fgets(filmList[index].producer, sizeof(filmList[index].producer), stdin);

        index ++;

        printf("\nFilm enregistrer avec succes\n");
    }

    printf("Appuyer sur une touche pour continuer..");
    getchar();
    system("cls");
}

static void printCollection(film filmList[50])
{
    // Display all the film from the collection
    printf("------------------------------\n");
    printf("| Affichage de la collection |\n");
    printf("------------------------------\n");

    printf("\n");

    for (int i=0; i<index; i++)
    {
        printf("--------------------\n");
        printf("| Film numero %d :  |\n", i+1);
        printf("--------------------\n");
        printf("    - Titre : %s", filmList[i].title);
        printf("    - Type : %s", filmList[i].type);
        printf("    - Sorti il y a : %hd an(s)\n", filmList[i].year);
        printf("    - Realise par : %s", filmList[i].producer);
        printf("    - Ce film apparait %hd fois dans la collection\n", filmList[i].number);
    }

    printf("\nAppuyer sur une touche pour continuer..");
    getchar();
    system("cls");
}

static void printFilm(film filmList[50])
{
    // Display the information from a specific film
    int found = 0;
    char titleToSearch[100];

    printf("-----------------------\n");
    printf("| Recherche d'un film |\n");
    printf("-----------------------\n");

    printf("Entrez le titre du film a rechercher : ");
    rewind(stdin);
    fgets(titleToSearch, sizeof(titleToSearch), stdin);

    printf("\n");

    for (int i=0; i<index; i++)
    {
        if (strcmp(filmList[i].title, titleToSearch) == 0)
        {
            printf("--------------------\n");
            printf("| Film numero %d :  |\n", i+1);
            printf("--------------------\n");
            printf("    - Titre : %s", filmList[i].title);
            printf("    - Type : %s", filmList[i].type);
            printf("    - Sorti il y a : %hd an(s)\n", filmList[i].year);
            printf("    - Realise par : %s", filmList[i].producer);
            printf("    - Ce film apparait %hd fois dans la collection\n", filmList[i].number);
            found = 1;
            break;
        }
    }

    if (found == 0)
    {
        printf("\nAucun film de ce nom n'a ete trouve", titleToSearch);
    }

    printf("\nAppuyer sur une touche pour continuer..");
    getchar();
    system("cls");
}

static void modifyFilm(film filmList[50])
{
    // Modify the number of copy of a specific film
    char titleToSearch[100];
    short newNumber;
    int found = 0;
    int res;

    printf("--------------------------------------------------\n");
    printf("| Modification du nombre d'exemplaires d'un film |\n");
    printf("--------------------------------------------------\n");

    // Ask for the film to modify
    printf("Entrez le titre du film a modifier : ");
    rewind(stdin);
    fgets(titleToSearch, sizeof(titleToSearch), stdin);

    // Ask the new number of copy
    do
    {
        printf("Entrez le nouveau nombre d'exemplaires : ");
        rewind(stdin);
        res = scanf("%hd", &newNumber);
        while ((getchar()) != '\n');

        if (res != 1 || newNumber < 1)
        {
            printf("Valeur entrez incorrect\n");
            newNumber = -1;
        }
    }while(newNumber < 0);

    // Look for the film in the collection and modify the number of copy
    for (int i=0; i<index; i++)
    {
        if (strcmp(filmList[i].title, titleToSearch) == 0)
        {
            filmList[i].number = newNumber;
            found = 1;
            break;
        }
    }

    if (found == 0)
    {
        printf("Aucun film de ce nom n'existe");
    }
    else
    {
        printf("\nModification effectue");
    }

    printf("\nAppuyer sur une touche pour continuer..");
    getchar();
    system("cls");
}

static void addNumberFilm(film filmList[50], char titleToSearch[100])
{
    // Auto incremente the number of copy when adding a new film that
    // already exist in the collection
    for (int i=0; i<index; i++)
    {
        if (strcmp(filmList[i].title, titleToSearch) == 0)
        {
            filmList[i].number++;
        }
    }
}

static void printCategory(film filmList[50])
{
    // Display all films from a specific category
    char typeToSearch[50];
    int found = 0;

    printf("-----------------------------\n");
    printf("| Recherche d'une categorie |\n");
    printf("-----------------------------\n");

    printf("Entrez la categorie de film a afficher : ");
    rewind(stdin);
    fgets(typeToSearch, sizeof(typeToSearch), stdin);

    printf("\n");

    for (int i=0; i<50; i++)
    {
        if (strcmp(filmList[i].type, typeToSearch) == 0)
        {
            printf("    - %s", filmList[i].title);
            found++;
        }
    }

    // If no film from the specific category have been found
    if (found == 0)
    {
        printf("\nAucun film de cette categorie n'a ete trouve", typeToSearch);
    }

    printf("\nAppuyer sur une touche pour continuer..");
    getchar();
    system("cls");
}

static void printDouble(film filmList[50])
{
    // Display all films that have number of copy of at least 2
    int found = 0;

    printf("--------------------------------------------\n");
    printf("| Affichage films de plus de 2 exemplaires |\n");
    printf("--------------------------------------------\n");

    for (int i=0; i<index; i++)
    {
        if (filmList[i].number >= 2)
        {
            printf("    - %s", filmList[i].title);
            found ++;
        }
    }

    // If no films with number of copy of at least 2 have been found
    if (found == 0)
    {
        printf("Aucun film n'existe plus de 2 fois");
    }

    printf("\nAppuyer sur une touche pour continuer..");
    getchar();
    system("cls");
}

static void printAscendingDateOrder(film filmList[50])
{
    printf("----------------------------------------------------\n");
    printf("| Affichage des films du plus recent au plus vieux |\n");
    printf("----------------------------------------------------\n");

    film filmTmp;
    film filmListCpy[50];

    for (int i=0; i<index; i++)
    {
        filmListCpy[i] = filmList[i];
    }

    for (int i=0; i<index-1; i++)
    {
        for (int j=0; j<index-i-1; j++)
        {
            if (filmListCpy[j].year > filmListCpy[j+1].year)
            {
                filmTmp = filmListCpy[j];
                filmListCpy[j] = filmListCpy[j+1];
                filmListCpy[j+1] = filmTmp;
            }
        }
    }

    printf("\n");

    for (int i=0; i<index; i++)
    {
        printf("--------------------\n");
        printf("| Film numero %d :  |\n", i+1);
        printf("--------------------\n");
        printf("    - Titre : %s", filmListCpy[i].title);
        printf("    - Type : %s", filmListCpy[i].type);
        printf("    - Sorti il y a : %hd an(s)\n", filmListCpy[i].year);
        printf("    - Realise par : %s", filmListCpy[i].producer);
        printf("    - Ce film apparait %hd fois dans la collection\n", filmListCpy[i].number);
    }

    printf("\nAppuyer sur une touche pour continuer..");
    getchar();
    system("cls");
}

static void printProducerFilms(film filmList[50])
{
    char producerToSearch[50];
    int found = 0;

    printf("------------------------------\n");
    printf("| Recherche d'un realisateur |\n");
    printf("------------------------------\n");

    printf("Entrez le realisateur de film a afficher : ");
    rewind(stdin);
    fgets(producerToSearch, sizeof(producerToSearch), stdin);

    printf("\n");

    for (int i=0; i<index; i++)
    {
        if (strcmp(filmList[i].producer, producerToSearch) == 0)
        {
            printf("   - %s", filmList[i].title);
            found++;
        }
    }

    if (found == 0)
    {
        printf("Aucun film de ce realisateur n'a ete trouve\n", producerToSearch);
    }

    printf("\Appuyer sur une touche pour continuer..");
    getchar();
    system("cls");
}
