#ifndef FILM_H
#define FILM_H

typedef struct film
{
    char title[100];
    short year;
    char type[50];
    short number;
    char producer[100];
}film;

// Functions
static void addFilm(film *filmList);
static void printCollection(film filmList[50]);
static void printFilm(film filmList[50]);
static void modifyFilm(film filmList[50]);
static void addNumberFilm(film filmList[50], char titleToSearch[100]);
static void printCategory(film filmList[50]);
static void printDouble(film filmList[50]);
static void printAscendingDateOrder(film filmList[50]);
static void printProducerFilms(film filmList[50]);

#endif // FILM_H
